//
//  SongCell.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

class AlbumCell:UITableViewCell{
    
    static let cellIdentifier = "AlbumCellIdentifier"
    
    let thumbnailImgView:UIImageView = UIImageView()
    let lbAlbumName:UILabel = UILabel()
    let lbArtistName:UILabel = UILabel()
    
    
    func configCell(album:Album){
        setupView()
        self.lbAlbumName.text = album.name
        self.lbArtistName.text = album.artistName
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let urlImage = URL(string: album.artworkUrl100) else{return}
            do{
                let data = try Data(contentsOf: urlImage)
                let thumbImage = UIImage(data: data)
                DispatchQueue.main.async { [weak self] in
                    self?.thumbnailImgView.image = thumbImage
                }
            }catch let error{
                debugPrint("error:\(album.artworkUrl100)-\(error.localizedDescription)")
            }
      
        }
        
    }
    
    private func setupView(){
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        thumbnailImgView.translatesAutoresizingMaskIntoConstraints = false
        lbArtistName.translatesAutoresizingMaskIntoConstraints = false
        lbAlbumName.translatesAutoresizingMaskIntoConstraints = false
        let margins = self.layoutMarginsGuide
        let screen = UIScreen.main.bounds
        self.contentView.addSubview(thumbnailImgView)
        self.contentView.addSubview(lbArtistName)
        self.contentView.addSubview(lbAlbumName)
    
        //self.contentView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        self.contentView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        
        thumbnailImgView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        thumbnailImgView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        thumbnailImgView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        thumbnailImgView.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 0).isActive = true
        thumbnailImgView.backgroundColor = UIColor.darkGray
        
        lbAlbumName.heightAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true
        lbAlbumName.leadingAnchor.constraint(equalTo: thumbnailImgView.trailingAnchor, constant: 8).isActive = true
        lbAlbumName.topAnchor.constraint(equalTo: margins.topAnchor, constant: 3).isActive = true
        lbAlbumName.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -8).isActive = true
        lbAlbumName.font = UIFont.boldSystemFont(ofSize: 18)
        lbAlbumName.numberOfLines = 2
        
        lbArtistName.heightAnchor.constraint(equalToConstant: 18).isActive = true
        lbArtistName.leadingAnchor.constraint(equalTo: thumbnailImgView.trailingAnchor, constant: 8).isActive = true
        lbArtistName.topAnchor.constraint(equalTo: lbAlbumName.bottomAnchor, constant: 5).isActive = true
        //lbAlbumName.widthAnchor.constraint(equalToConstant: screen.width-16).isActive = true
        lbArtistName.font = UIFont.systemFont(ofSize: 16)
        self.layoutIfNeeded()
        
    }
}
