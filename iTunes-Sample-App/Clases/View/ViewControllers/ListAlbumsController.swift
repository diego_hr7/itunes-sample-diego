//
//  ListSongsController.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

protocol ListAlbumsViewable {
    func willDisplayAlbums(albumsDownloaded:[Album]?)
    func showAlert(message:String)
    func pushController(controller:UIViewController)
}

class ListAlbumsController:UIViewController{
    
    var albumsPresenter:ListAlbumsPresentable!
    private var tableAlbums:UITableView = UITableView()
    private let refreshControl = UIRefreshControl()
    private var albums:[Album] = []{
        didSet{
            DispatchQueue.main.async {
                self.tableAlbums.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.tableAlbums.register(AlbumCell.self, forCellReuseIdentifier: AlbumCell.cellIdentifier)
        self.tableAlbums.delegate = self
        self.tableAlbums.dataSource = self
        self.albumsPresenter = ListAlbumPresenter(view: self)
        self.reloadData()
    }
    
    private func setupView(){
        self.title = "Albums"
        self.view.backgroundColor = UIColor.white
        tableAlbums.translatesAutoresizingMaskIntoConstraints = false
        let screen = UIScreen.main.bounds
        self.view.addSubview(tableAlbums)
        tableAlbums.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        tableAlbums.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        tableAlbums.widthAnchor.constraint(equalToConstant: screen.width).isActive = true
        tableAlbums.heightAnchor.constraint(equalToConstant: screen.height).isActive = true
        
        refreshControl.addTarget(self, action: #selector(self.reloadData), for: .valueChanged)
        tableAlbums.addSubview(refreshControl)
    }
    
    @objc func reloadData(){
        UIUtilities.shared.showSimpleActivityIndicator()
        self.albumsPresenter.reloadAlbums()
    }
}

extension ListAlbumsController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AlbumCell = AlbumCell()
        let album = albums[indexPath.row]
        cell.configCell(album: album)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.albumsPresenter.selectAlbum(album: albums[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension ListAlbumsController:ListAlbumsViewable{
    func willDisplayAlbums(albumsDownloaded:[Album]?){
        UIUtilities.shared.stopActivityIndicator()
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
        }
        if let albums = albumsDownloaded{
            self.albums = albums
        }
    }
    
    func showAlert(message:String){
        UIUtilities.showSimpleAlert(title: nil, message: message, controller: self)
    }
    
    func pushController(controller:UIViewController){
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
