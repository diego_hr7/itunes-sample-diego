//
//  DetailAlbumController.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

protocol DetailAlbumViewable {
    func displayInformation(album:Album)
    func showAlert(message:String)
    func openUrl(url:URL)
}

class DetailAlbumController:UIViewController{
    var detailAlbumPresenter:DetailAlbumPresentable!
    var album:Album!
    
    private var albumImg = UIImageView()
    private var albumNameView = DescriptionDetailView()
    private var artistNameView = DescriptionDetailView()
    private var genresView = DescriptionDetailView()
    private var releaseDateView = DescriptionDetailView()
    private var copyRight = UILabel()
    private var btnItunes = UIButton()
    
    
    override func viewDidLoad() {
        detailAlbumPresenter = DetailAlbumPresenter(view: self)
        self.setupView()
    }
    
    private func setupView(){
        self.title = "Album Detail"
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(albumImg)
        self.view.addSubview(albumNameView)
        self.view.addSubview(artistNameView)
        self.view.addSubview(genresView)
        self.view.addSubview(releaseDateView)
        self.view.addSubview(copyRight)
        self.view.addSubview(btnItunes)
        let screen = UIScreen.main.bounds
        
        let margins = self.view.layoutMarginsGuide
        albumImg.translatesAutoresizingMaskIntoConstraints = false
        albumNameView.translatesAutoresizingMaskIntoConstraints = false
        artistNameView.translatesAutoresizingMaskIntoConstraints = false
        genresView.translatesAutoresizingMaskIntoConstraints = false
        releaseDateView.translatesAutoresizingMaskIntoConstraints = false
        copyRight.translatesAutoresizingMaskIntoConstraints = false
        btnItunes.translatesAutoresizingMaskIntoConstraints = false
        self.view.layoutIfNeeded()
        
        albumImg.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        albumImg.topAnchor.constraint(equalTo: margins.topAnchor, constant: 10).isActive = true
        albumImg.widthAnchor.constraint(equalToConstant: 150).isActive = true
        albumImg.heightAnchor.constraint(equalToConstant: 150).isActive = true
        albumImg.backgroundColor = UIColor.darkGray
        
        albumNameView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        albumNameView.topAnchor.constraint(equalTo: albumImg.bottomAnchor, constant: 20).isActive = true
        albumNameView.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 8).isActive = true
        albumNameView.widthAnchor.constraint(equalToConstant: screen.width - 16).isActive = true
        albumNameView.heightAnchor.constraint(greaterThanOrEqualToConstant: 25).isActive = true
        
        artistNameView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        artistNameView.topAnchor.constraint(equalTo: albumNameView.bottomAnchor, constant: 15).isActive = true
        artistNameView.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 8).isActive = true
        artistNameView.widthAnchor.constraint(equalToConstant: screen.width - 16).isActive = true
        artistNameView.heightAnchor.constraint(greaterThanOrEqualToConstant: 25).isActive = true

        genresView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        genresView.topAnchor.constraint(equalTo: artistNameView.bottomAnchor, constant: 15).isActive = true
        genresView.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 8).isActive = true
        genresView.heightAnchor.constraint(greaterThanOrEqualToConstant: 25).isActive = true
        
        
        releaseDateView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        releaseDateView.topAnchor.constraint(equalTo: genresView.bottomAnchor, constant: 15).isActive = true
        releaseDateView.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 8).isActive = true
        releaseDateView.heightAnchor.constraint(greaterThanOrEqualToConstant: 25).isActive = true
        
        copyRight.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        copyRight.topAnchor.constraint(equalTo: releaseDateView.bottomAnchor, constant: 15).isActive = true
        copyRight.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 8).isActive = true
        copyRight.heightAnchor.constraint(lessThanOrEqualToConstant: 120).isActive = true
        copyRight.numberOfLines = 5
        
        btnItunes.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        btnItunes.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: -20).isActive = true
        btnItunes.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 20).isActive = true
        btnItunes.heightAnchor.constraint(equalToConstant: 30).isActive = true
        btnItunes.setTitle("Go iTunes", for: .normal)
        btnItunes.addTarget(self, action: #selector(goItunes), for: .touchUpInside)
        btnItunes.backgroundColor = UIColor.blue
        
        self.view.layoutIfNeeded()
        debugPrint(btnItunes)
        self.displayInformation(album: self.album)
        
    }
    
    @objc func goItunes(){
        self.detailAlbumPresenter.goToItunes(itunesUrl: album.artistUrl)
    }
        
    
}

extension DetailAlbumController:DetailAlbumViewable{
    func openUrl(url: URL) {
        UIApplication.shared.open(url)
    }
    
    func displayInformation(album: Album) {
        albumNameView.setView(hint: "Name", value: album.name)
        artistNameView.setView(hint: "Artist", value: album.artistName)
        genresView.setView(hint: "Genres", value: album.getGenresFormatted())
        releaseDateView.setView(hint: "Release date", value: album.releaseDate)
        copyRight.text = album.copyright
        
        DispatchQueue.global(qos: .default).async { [weak self] in
            guard let urlImage = URL(string: album.artworkUrl100) else{return}
            do{
                let data = try Data(contentsOf: urlImage)
                let thumbImage = UIImage(data: data)
                DispatchQueue.main.async { [weak self] in
                    self?.albumImg.image = thumbImage
                }
            }catch let error{
                debugPrint("error:\(album.artworkUrl100)-\(error.localizedDescription)")
            }
            
        }
    }
    
    func showAlert(message:String){
        DispatchQueue.main.async {
            UIUtilities.showSimpleAlert(title: nil, message: message, controller: self)
        }
    }
    
}
