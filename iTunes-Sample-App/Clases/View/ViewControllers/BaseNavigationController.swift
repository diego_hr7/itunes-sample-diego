//
//  BaseNavigationController.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/20/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

class BaseNavigationController:UINavigationController{
    
    func initItunesSample(){
        let listAlbumsControlle = ListAlbumsController()
        self.setViewControllers([listAlbumsControlle], animated: true)
    }
    
}
