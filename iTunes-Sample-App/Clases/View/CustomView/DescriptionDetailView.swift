//
//  DescriptionDetail.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

class DescriptionDetailView:UIView{
    
    var lbHint = UILabel()
    var lbValue = UILabel()
    
    func setView(hint:String, value:String) {
        self.setUpView()
        self.lbHint.text = hint + ": "
        self.lbValue.text = value
    }
    
    func setUpView(){
        self.addSubview(lbHint)
        self.addSubview(lbValue)
        let screen = UIScreen.main.bounds
        let margins = self.layoutMarginsGuide
        
        lbHint.translatesAutoresizingMaskIntoConstraints = false
        lbValue.translatesAutoresizingMaskIntoConstraints = false
        
        lbHint.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        lbHint.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 0).isActive = true
        lbHint.heightAnchor.constraint(equalToConstant: 20).isActive = true
        lbHint.widthAnchor.constraint(greaterThanOrEqualToConstant: screen.width/4.0).isActive = true
        //lbHint.widthAnchor.constraint(greaterThanOrEqualToConstant: screen.width/4.0).isActive = true
        lbHint.font = UIFont.systemFont(ofSize: 16)
        
        lbValue.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        lbValue.leadingAnchor.constraint(equalTo: lbHint.trailingAnchor, constant: 0).isActive = true
        lbValue.heightAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true
        lbValue.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: 0).isActive = true
        lbValue.font = UIFont.boldSystemFont(ofSize: 16)
        lbValue.numberOfLines = 2
        self.layoutIfNeeded()
    }
}
