//
//  Date+Extension.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

extension Date{
    enum DateFormatCustom:String{
        case DDMMYYY = "dd-MM-yyyy"
    }
    
    func parseDateWithFormat(format:DateFormatCustom) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        return dateFormatter.string(from: self)
    }
}
