//
//  Song.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation


struct Album:Decodable{
    var artistName:String
    var songId:String
    var releaseDate:String
    var name:String
    var kind:String
    var copyright:String
    var artistId:String
    var artistUrl:String
    var artworkUrl100:String
    var songUrl:String
    var genres:[Genre] = []
    
    private enum CodingKeys: String, CodingKey {
        case artistName, songId = "id", releaseDate, name, kind, copyright, artistId, artistUrl, artworkUrl100, songUrl = "url", genres
    }
    
    func getGenresFormatted() -> String{
        var genresFormatted:String = ""
        for genre in self.genres{
            genresFormatted.append(genre.name + ",")
        }
        return String(genresFormatted.dropLast())
    }
}

