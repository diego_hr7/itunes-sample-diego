//
//  Genre.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

struct Genre:Decodable{
    var genreId:String
    var name:String
    var url:String
    
    private enum CodingKeys: String, CodingKey {
        case genreId, name, url
    }
}
