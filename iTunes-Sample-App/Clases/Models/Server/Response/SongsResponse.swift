//
//  SongsResponse.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

struct SongsResponse:Decodable{
    var feed:Feed
    
    private enum CodingKeys: String, CodingKey {
        case feed
    }
    
}
struct Feed:Decodable {
    var title:String
    var id:String
    var author:Author
    var links:[[String:String]]
    var copyright:String
    var country:String
    var icon:String
    var updated:String
    var results:[Album]
    
    private enum CodingKeys: String, CodingKey {
        case title, id, author, links, copyright, country, icon, updated, results
    }
}

struct Author:Decodable {
    var name:String
    var uri:String
    
    private enum CodingKeys: String, CodingKey {
        case name, uri
    }
}

