//
//  API.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

class Connection{
    private typealias MethodAPI = String
    
    private enum methodsAPI:MethodAPI{
        case getSongs = "https://rss.itunes.apple.com/api/v1/us/apple-music/coming-soon/all/100/explicit.json"
    }
    
    public static let shared:Connection = Connection()
    
    func getSongs(successClosure: (SuccessHandler)? = nil, errorClosure: (ErrorHandler)? = nil){
        let url = URL(string: methodsAPI.getSongs.rawValue)!
        let task = URLSession.shared.dataTask(with: url) { (data, responsee, error) in
            if let errorConnection = error {
                errorClosure!(errorConnection, errorConnection.localizedDescription)
            } else {
                do {
                    if let data = data{
                        let jsonDecoder: JSONDecoder = JSONDecoder()
                        let response = try jsonDecoder.decode(SongsResponse.self, from: data)
                        successClosure!(response)
                    }else{
                        errorClosure!(nil, "Error deserializing JSON")
                    }
                }catch let parseError{
                    errorClosure!(parseError, parseError.localizedDescription)
                }
            }
        }
        task.resume()
    }
}

extension Connection{
    public typealias SuccessHandler = (Decodable) -> Void
    public typealias ErrorHandler = (Error?, String) -> Void
}


