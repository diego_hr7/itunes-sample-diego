//
//  UIUtilities.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

class UIUtilities{
    static let shared = UIUtilities()
    
    private var containerView:UIView!
    
    static func showSimpleAlert(title:String?, message:String, controller:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        DispatchQueue.main.async {
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    func showSimpleActivityIndicator(){
        DispatchQueue.main.async {
            self.containerView = UIView(frame: UIScreen.main.bounds)
            self.containerView.backgroundColor = UIColor.clear
            let grayView = UIView(frame: UIScreen.main.bounds)
            grayView.backgroundColor = UIColor.darkGray
            grayView.alpha = 0.7
            let activityIndicator = UIActivityIndicatorView()
            activityIndicator.color = UIColor.white
            self.containerView.addSubview(grayView)
            self.containerView.addSubview(activityIndicator)
            
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            activityIndicator.centerXAnchor.constraint(equalTo: self.containerView.centerXAnchor).isActive = true
            activityIndicator.centerYAnchor.constraint(equalTo: self.containerView.centerYAnchor).isActive = true
            activityIndicator.widthAnchor.constraint(equalToConstant: 30).isActive = true
            activityIndicator.heightAnchor.constraint(equalToConstant: 30).isActive = true
            activityIndicator.startAnimating()
            UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self.containerView)
        }
        
        
    }
    
    func stopActivityIndicator(){
        DispatchQueue.main.async {
            self.containerView?.removeFromSuperview()
            self.containerView = nil
        }
        
    }
}
