//
//  ListAlbumPresenter.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

protocol ListAlbumsPresentable{
    func reloadAlbums()
    func selectAlbum(album:Album)
    init(view:ListAlbumsViewable)
}

class ListAlbumPresenter:ListAlbumsPresentable{
    var viewAlbum:ListAlbumsViewable!
    
    required init(view: ListAlbumsViewable) {
        self.viewAlbum = view
    }
    
    func reloadAlbums(){
        Connection.shared.getSongs(successClosure: { (response) in
            guard let albumsResponse = response as? SongsResponse else{return}
            self.viewAlbum.willDisplayAlbums(albumsDownloaded: albumsResponse.feed.results)
        }) { (error, errorMessag) in
            self.viewAlbum.willDisplayAlbums(albumsDownloaded: nil)
            self.viewAlbum.showAlert(message: errorMessag)
        }
    }
    
    func selectAlbum(album:Album){
        let detailAlbumController = DetailAlbumController()
        detailAlbumController.album = album
        self.viewAlbum.pushController(controller: detailAlbumController)
    }
}


