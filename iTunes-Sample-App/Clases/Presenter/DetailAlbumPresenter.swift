//
//  DetailAlbumPresenter.swift
//  iTunes-Sample-App
//
//  Created by Armit Solutions on 10/19/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

protocol DetailAlbumPresentable{
    func goToItunes(itunesUrl:String)
    init(view:DetailAlbumViewable)
}

class DetailAlbumPresenter:DetailAlbumPresentable{
    var detailView:DetailAlbumViewable!
    func goToItunes(itunesUrl: String) {
        if let url = URL(string: itunesUrl) {
            self.detailView.openUrl(url: url)
        }else{
            self.detailView.showAlert(message: "There was an error opening iTunes")
        }
    }
    
    required init(view: DetailAlbumViewable) {
        self.detailView = view
    }
    
    
}
